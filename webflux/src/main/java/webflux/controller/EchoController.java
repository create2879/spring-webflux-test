package webflux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;
import webflux.domain.model.EchoModel;
import webflux.service.EchoService;

@RestController
public class EchoController {
	
	@Autowired
	private EchoService echoService;

	@GetMapping(path = "/echo/two")
	public Mono<EchoModel> echoTwoGet(@RequestParam String echo) {
		return Mono.just(echoService.getEchoModel(echo));
	}
	
	@PostMapping(path = "/echo/two")
	public Mono<EchoModel> echoTwoPost(@RequestBody EchoModel model) {
		return Mono.just(model);
	}
}
