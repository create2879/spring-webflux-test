package webflux.domain.model;

public class EchoModel {

	private String echo;
	
	private EchoModel() {}
	
	public static EchoModel of(String echo) {
		EchoModel model = new EchoModel();
		model.echo = echo;
		return model;
	}

	public String getEcho() {
		return echo;
	}
	
}
