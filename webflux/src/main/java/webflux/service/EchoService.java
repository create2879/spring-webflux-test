package webflux.service;

import org.springframework.stereotype.Service;

import webflux.domain.model.EchoModel;

@Service
public class EchoService {

	public EchoModel getEchoModel(String message) {
		return EchoModel.of(message);
	}
}
