package webflux.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.DelegatingWebFluxConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import webflux.handler.EchoHandler;

@EnableWebFlux
@Configuration
public class WebConfig extends DelegatingWebFluxConfiguration{

	@Bean
	public RouterFunction<?> webFluxTestRouter(EchoHandler echoHandler) {
		return RouterFunctions.route(RequestPredicates.GET("/echo/one"), echoHandler::echoOneGet)
				.andRoute(RequestPredicates.POST("/echo/one"), echoHandler::echoOnePost);
    }
}
