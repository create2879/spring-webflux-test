package webflux.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;
import webflux.domain.model.EchoModel;
import webflux.service.EchoService;

@Component
public class EchoHandler {

	@Autowired
	private EchoService echoService;

	public Mono<ServerResponse> echoOneGet(ServerRequest request) {
		return ServerResponse.ok().body(Mono.just(echoService.getEchoModel(request.queryParam("echo").orElse(""))),
				EchoModel.class);
	}

	public Mono<ServerResponse> echoOnePost(ServerRequest request) {
		return ServerResponse.ok().body(request.bodyToMono(EchoModel.class), EchoModel.class);
	}
}
